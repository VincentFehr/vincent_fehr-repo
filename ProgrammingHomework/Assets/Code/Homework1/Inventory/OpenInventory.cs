﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UEGP3.CameraSystem
{
    public class OpenInventory : MonoBehaviour
    {
        [Tooltip("The GameObjects that I want to activate/deactivate to show the inventory")]
        [SerializeField]
        private GameObject _inventory;
        [SerializeField]
        private GameObject _potionBag;
        [SerializeField]
        private GameObject _descriptionTextBox;

        // Update is called once per frame
        void Update()
        {
            // TODO 
            // yes, good job! Small annotation: You could've done this in the existing player class too, where we already output the string of the inventory content
            // Also, here again: if (aBool == true/false) should be simplified to if (aBool/!aBool) for better readability
            // You also added a bunch of empty lines, which should usually be removed.
            
            //I open the Inventory with the in unity assigned button named "Inventory"
            if (Input.GetButtonDown("Inventory") && _inventory.activeInHierarchy == false && _potionBag.activeInHierarchy == false)
            {
                _inventory.SetActive(true);
                _descriptionTextBox.SetActive(true);


            }
            else if (Input.GetButtonDown("Inventory") && _inventory.activeInHierarchy == true && _potionBag.activeInHierarchy == false)
            {
                _inventory.SetActive(false);
                _descriptionTextBox.SetActive(false);


            }
            else if (Input.GetButtonDown("Inventory") && _inventory.activeInHierarchy == true && _potionBag.activeInHierarchy == true)
            {
                _inventory.SetActive(false);
                _potionBag.SetActive(false);
                _descriptionTextBox.SetActive(false);

            }
        }
    }
}
