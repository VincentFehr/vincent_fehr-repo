﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


namespace UEGP3.InventorySystem
{
    public class Tooltip : MonoBehaviour
    {
        [Header("Textboxes")]
        [Tooltip("Reference to the text box that is going to be our description box")]
        [SerializeField]
        private TextMeshProUGUI _tooltipText;
        [Tooltip("Reference to the text box that is going to be our name box")]
        [SerializeField]
        private TextMeshProUGUI _nameText;

        [Tooltip("The button that uses an item that you clicked on")]
        [SerializeField]
        private Button _button;

        [SerializeField]
        private Item _item;

        [SerializeField]
        private Inventory _inventory;

        private HealItem _potion;

        // TODO
        // Whenever we call a method via some magical unity event, like the on click of a button, without adding the usage via code, we should annotate
        // the method with a note that tells "Hey, this is used by an event!" when we look at it. Otherwise, code that seems unused might get removed
        // and we break our game. I usually do something like this:
        // Called by PotionSlot Buttons OnClick event. Check before removing.
        public void InfoTextPotionBag()
        {
            // TODO 
            // if we compare two strings, we usually use myString.Equals("hello world"). This is, because strings are reference types, not value types.
            // if you want I can give you something to read up on this. In the case of tag comparison in unity, we even have a dedicated method for this:
            // gameObject.CompareTag("tag");
            // I would also suggest to rather check for the item type, then tagging each item. Otherwise you will end with an unreadable mess of tags pretty fast
            // ( I know that from experience and let me tell you: you don't wanna be there!)
            
            //This method can be used for items that never change and are saved in a specific way i.e. stored in a place no other item has access to which each item having their respective slot
            if (this.gameObject.name == "ItemXYZ")
            {
                _tooltipText.text = "ItemXYZ \nThis item does XYZ";
            }
            if (this.gameObject.tag == "PotionSlot")
            {                
                _tooltipText.text = "Health Potion \nRegenerate 10 health per second. Lasts 5 seconds.";
            }
            if (this.gameObject.tag == "DamagePotion")
            {
                _tooltipText.text = "Damage Potion \nIncreases damage dealt by 20. Lasts 10 seconds.";
            }
            if (this.gameObject.tag == "SpeedPotion")
            {
                _tooltipText.text = "Speed Potion \nGrants a small speed boost after consumption. Lasts 30 seconds.";
            }      
        }

        private void Update()
        {
            
        }

       

        private void OnMouseDown(Button _button)
        {
            if(gameObject.tag == "InventorySlots")
            {
                InfoTextInventory();
            }
            
        }

        public void InfoTextInventory()
        {
            _tooltipText.text = _item.Description;
        }

        // Start is called before the first frame update
        void Start()
        {
            if(gameObject.tag == "InventorySlot")
            {
                _button = GetComponentInChildren<Button>();
                _nameText = GetComponentInChildren<TextMeshProUGUI>();
            }
           
            _tooltipText.text = "For more information, hover over an item!";
        }

    }
}

