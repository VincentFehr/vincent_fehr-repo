﻿using System;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(CharacterController))]
	public class PlayerController : MonoBehaviour
	{
		[Header("General Settings")]
        [Tooltip("The speed with which the player moves forward")] [SerializeField]
		private float _movementSpeed = 10f;
		[Tooltip("The graphical represenation of the character. It is used for things like rotation")] [SerializeField]
		private Transform _graphicsObject = null;
		[Tooltip("Reference to the game camera")] [SerializeField]
		private Transform _cameraTransform = null;

		[Header("Movement")]
        [Tooltip("Smoothing time for turns")] [SerializeField]
		private float _turnSmoothTime = 0.15f;
		[Tooltip("Smoothing time to reach target speed")] [SerializeField]
		private float _speedSmoothTime = 0.7f;
		[Tooltip("Modifier that manipulates the gravity set in Unitys Physics settings")] [SerializeField]
		private float _gravityModifier = 1.0f;
		[Tooltip("Maximum falling velocity the player can reach")] [Range(1f, 15f)] [SerializeField]
		private float _terminalVelocity = 10f;
		[Tooltip("The height in meters the cahracter can jump")] [SerializeField]
		private float _jumpHeight;
        [Tooltip("The distance of the dash into the forward direction")]
        [SerializeField]
        private int _dashDistance;
        // TODO
        // We don't need tooltips for non-serialized fields, they can't be inspected anyways. A regular comment should be sufficient. Anyways, good job on commenting your newly introduced variable!
        // You're actually one of few people who did this!
        [Tooltip("The vector of the dash into the forward direction")]
        private Vector3 _dash;
        [Tooltip("A value to increase/decrease the ability to change direction while in the air")] [Range(0f, 1f)] [SerializeField]
        private float _airStrafeValue;

        [Header("Ground Check")]
        [Tooltip("A transform used to detect the ground")] [SerializeField]
		private Transform _groundCheckTransform = null;
		[Tooltip("The radius around transform which is used to detect the ground")] [SerializeField]
		private float _groundCheckRadius = 0.1f;
		[Tooltip("A layermask used to exclude/include certain layers from the \"ground\"")] [SerializeField]
		private LayerMask _groundCheckLayerMask = default;

		// Use formula: Mathf.Sqrt(h * (-2) * g)
		private float JumpVelocity => Mathf.Sqrt(_jumpHeight * -2 * Physics.gravity.y);
		
		private bool _isGrounded;
		private float _currentVerticalVelocity;
		private float _currentForwardVelocity;
		private float _speedSmoothVelocity;
		private CharacterController _characterController;
		private PlayerAnimationHandler _playerAnimationHandler;
        private Vector3 _velocity;
		
		private void Awake()
		{
			_characterController = GetComponent<CharacterController>();
			_playerAnimationHandler = GetComponent<PlayerAnimationHandler>();
		}

		private void Update()
		{
			// Fetch inputs
			// GetAxisRaw : -1, +1 (0) 
			// GetAxis: [-1, +1]
			float horizontalInput = Input.GetAxisRaw("Horizontal");
			float verticalInput = Input.GetAxisRaw("Vertical");
			bool jumpDown = Input.GetButtonDown("Jump");
            bool dashDown = Input.GetButtonDown("Dash");

            // Calculate a direction from input data 
            Vector3 direction = new Vector3(horizontalInput, 0, verticalInput).normalized;
			
			// If the player has given any input, adjust the character rotation
			if (direction != Vector3.zero)
			{
				float lookRotationAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _cameraTransform.eulerAngles.y;
				Quaternion targetRotation = Quaternion.Euler(0, lookRotationAngle, 0);

				// TODO
				// this can be simplified: if (aBool) and if (aBool == true) are the same. The former is the preferred version in 99% of the use-cases
				// You also did this correct in the second part, with if (!aBool), which would be the same as writing (aBool == false)
				// The whole statement can be simplified by using if-else instead, as booleans only have two states (true/false) anyways.
                if(_isGrounded == true)
                {
                    _graphicsObject.rotation = Quaternion.Slerp(_graphicsObject.rotation, targetRotation, _turnSmoothTime);

                }
                else if(!_isGrounded)
                {
	                // Valid implementation. I wrote a method to calculate the smooth time, respecting air control. Its copied to the bottom of the project for you to inspect and play around with.
                    _graphicsObject.rotation = Quaternion.Slerp(_graphicsObject.rotation, targetRotation, _turnSmoothTime * _airStrafeValue);
                }
                              
			}

			// Calculate velocity based on gravity formula: delta-y = 1/2 * g * t^2
			// We ignore the 1/2 to safe multiplications and because it feels better.
			// Second Time.deltaTime is done in controller.Move()-call so we save one multiplication here.
			_currentVerticalVelocity += Physics.gravity.y * _gravityModifier * Time.deltaTime;
			
			// Clamp velocity to reach no more than our defined terminal velocity
			_currentVerticalVelocity = Mathf.Clamp(_currentVerticalVelocity, -_terminalVelocity, JumpVelocity);

			// Calculate velocity vector based on gravity and speed
			// (0, 0, z) -> (0, y, z)
			float targetSpeed = (_movementSpeed * direction.magnitude);
                       
            _currentForwardVelocity = Mathf.SmoothDamp(_currentForwardVelocity, targetSpeed, ref _speedSmoothVelocity, _speedSmoothTime);
                    
            _velocity = _graphicsObject.forward * _currentForwardVelocity + Vector3.up * _currentVerticalVelocity;
            
          
            
			
			// Use the direction to move the character controller
			// direction.x * Time.deltaTime, direction.y * Time.deltaTime, ... -> resultingDirection.x * _movementSpeed
			// Time.deltaTime * _movementSpeed = res, res * direction.x, res * direction.y, ...
			_characterController.Move(_velocity * Time.deltaTime);
			
			// Check if we are grounded, if so reset gravity
			_isGrounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundCheckLayerMask);
			if (_isGrounded)
			{
				// Reset current vertical velocity
				_currentVerticalVelocity = 0f;
			}

			// If we are grounded and jump was pressed, jump
			if (_isGrounded && jumpDown)
			{
				_currentVerticalVelocity = JumpVelocity;
			}

            
			_playerAnimationHandler.SetMovementSpeed(_currentForwardVelocity);

			// TODO 
			// this works but it gives the feel that the dash is very jumpy (which can be okay too, its more a teleport-style dash then though)
			// The jumpiness comes from the second _characterController Move statement. It is advisable to call it just once, and do all modifications 
			// of the movement vector beforehand. So in this case, we would simply move the computation for this up. In fact, we can do it right after
			// we determined our target speed and before we assign the _currentForwardVelocity. It is similar to the jump velocity logic: Calculate 
			// the velocity based on the formula g * -2 * distance and directly set the velocity. 
            _dash = _graphicsObject.forward * _dashDistance;
            if(_isGrounded && dashDown)
            {
	            // these are fine for debugging, but should be removed once no longer used/required. Debug.Log costs quite some CPU time, so we'd better get
	            // rid of them or our game might run slow some day!
                Debug.Log("I am dashing");
                _characterController.Move(_dash * Time.deltaTime);

            }
		}
		
		// TODO 
		// check this out
		private float _airControl; // <- this would be the serialized private field, you can also just replace it with yours. 
		/// <summary>
		/// Calculates the smoothTime based on airControl.
		/// </summary>
		/// <param name="smoothTime">The initial smoothTIme</param>
		/// <param name="zeroControlIsMaxValue">If we do not have air control, is the smooth time float.MaxValue or float.MinValue?</param>
		/// <returns>The smoothTime after regarding air control</returns>
		private float GetSmoothTimeAfterAirControl(float smoothTime, bool zeroControlIsMaxValue)
		{
			// We are grounded, don't modify smoothTime
			if (_characterController.isGrounded)
			{
				return smoothTime;
			}

			// Avoid divide by 0 exception
			if (Math.Abs(_airControl) < Mathf.Epsilon)
			{
				return zeroControlIsMaxValue ? float.MaxValue : float.MinValue;
			}

			// smoothTime is influenced by air control
			return smoothTime / _airControl;
		}
	}
}
